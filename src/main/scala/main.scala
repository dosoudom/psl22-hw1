package cz.cvut.fit.dosoudom

import scala.annotation.tailrec

def spiral(n: Int): TextElement = {
  assert(n >= 1)
  if (n == 1) CharElement('*', 1, 1)
  else
    n % 4 match
      case 0 => spiral(n - 1).expandIfThinnerThan(n, HorizontalDir.Left).above(CharElement('*', n, 1))
      case 1 => CharElement('*', 1, n).beside(spiral(n - 1).expandIfLowerThan(n, VerticalDir.Up))
      case 2 => CharElement('*', n, 1).above(spiral(n - 1))
      case 3 => spiral(n - 1).beside(CharElement('*', 1, n))
}

@main
def main(): Unit = {

  println(spiral(4).expandIfLowerThan(5, VerticalDir.Up).toString)
  println()
  println(spiral(5).toString)
}