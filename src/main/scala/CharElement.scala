package cz.cvut.fit.dosoudom

class CharElement(val char: Char, width: Int, height: Int) extends TextElement {
  override val content: List[String] = List.fill(height)(char.toString * width)
}
