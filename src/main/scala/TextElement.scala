package cz.cvut.fit.dosoudom

import scala.math.max

enum HorizontalDir {
  case Left, Right
}

enum VerticalDir {
  case Up, Down
}

trait TextElement {
  val content: List[String]

  def width: Int = content.headOption.map(_.length).getOrElse(0)

  def height: Int = content.length

  override def toString: String = content.mkString("\n")

  def expandIfThinnerThan(requiredWidth: Int, dir: HorizontalDir = HorizontalDir.Right): TextElement = {
    val expansion = " " * max(0, requiredWidth - this.width)
    BE(
      this.content.map(line => if (dir == HorizontalDir.Left) { expansion ++ line } else { line ++ expansion })
    )
  }

  def expandIfLowerThan(requiredHeight: Int, dir: VerticalDir = VerticalDir.Down): TextElement = {
    val expansion = List.fill(max(0, requiredHeight - this.height))(" " * this.width)
    if (dir == VerticalDir.Down) {
      BE(this.content ++ expansion)
    } else {
      BE(expansion ++ this.content)
    }
  }

  def above(that: TextElement): TextElement = {
    BE(
      this.expandIfThinnerThan(that.width).content ++ that
        .expandIfThinnerThan(this.width)
        .content
    )
  }

  def beside(that: TextElement): TextElement = {
    BE(
      this.content
        .zipAll(that.content, " " * this.width, " " * that.width)
        .map((thisLine, thatLine) => thisLine + thatLine)
    )
  }
}

class BE(override val content: List[String]) extends TextElement
