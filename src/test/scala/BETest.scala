package cz.cvut.fit.dosoudom

import org.scalatest.flatspec.AnyFlatSpec

class BETest extends AnyFlatSpec {
  it should "something something" in {
    assert(BE(List("foo")).toString == "foo")
  }

  it should "put one element above other" in {
    assert(BE(List("foo")).above(BE(List("bar"))).toString == "foo\nbar")
  }

  it should "put one element next to other" in {
    assert(
      BE(List("foo", "baz"))
        .beside(BE(List("bar", "yum")))
        .toString == "foobar\nbazyum"
    )
  }

  it should "handle different sizes in beside" in {
    assert(
      BE(List("foo", "bar"))
        .beside(BE(List("baz")))
        .toString == "foobaz\nbar   "
    )
    assert(
      BE(List("foo"))
        .beside(BE(List("bar", "baz")))
        .toString == "foobar\n   baz"
    )
  }

  it should "handle different sizes in above" in {
    assert(
      BE(List("foo"))
        .above(BE(List("ba")))
        .toString == "foo\nba "
    )
    assert(
      BE(List("fo"))
        .above(BE(List("bar")))
        .toString == "fo \nbar"
    )
  }
}
